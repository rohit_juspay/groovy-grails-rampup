import groovy.transform.*

@Canonical(excludes="firstName, lastName")
class Person {
int age
String firstName
String lastName
}

Person a = new Person()
a.age = 20
a.firstName = 'Rohit'
a.lastName = 'Ratnakumar'
println a.toString()

//if you use def, needn't write the type explicitly
def sara = new Person(age: 30, firstName: 'aa', lastName: 'bb')
println sara