def splitName(fullName) { fullName.split(' ') } //need to return an array
def (firstName, lastName) = splitName('James Bond')
println "$lastName, $firstName $lastName"

name1 = 'aaaaa'
name2 = 'bbb'
print "\n $name1 $name2 \n"
(name1, name2) = [name2, name1]
print "\n $name1 $name2 \n"

def (name1, name2) = [name2, name1]
print "\n $name1 $name2 \n"