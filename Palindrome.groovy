String.metaClass.isPalindrome = { ->
    delegate == delegate.reverse();
}

word = 'aaa';
println "${word.isPalindrome()}"
println word
println 'word'
println "word"
println word.length()
println word.isPalindrome()