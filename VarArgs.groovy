
def v1 (int a, int[] b) 
{
println "v1: $a $b"
}

//var args
def v2 (int a, int ... b)
{
println "v2: $a $b"
}

v1(1, 2, 3, 4, 5)
v2(1, 2, 3, 4, 5)

//v1(1, [2, 3, 4, 5]) //invalid, sending as array list
//v2(1, [2, 3, 4, 5]) //invalid

int[] arr = [2, 3, 4, 5]
v1(1, arr)
v1(1, arr as int[])
v1(1, [2, 3, 4, 5] as int[])

v2(1, arr)
v2(1, arr as int[])
v2(1, [2, 3, 4, 5] as int[])



