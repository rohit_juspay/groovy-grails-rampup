/*print "#1" example
class Car {

}

Car a = new Car()

*/

/* readonly propery
println "#2"
class Car2 {
final year //readonly

Car2(thisyear){ year = thisyear}
}

Car2 b = new Car2(2008)
print b.year
b.year = 2009 // gives readonly property exception
*/

/* normal setting
println "#3"
class Car3 {

def miles = 0

}

Car3 c = new Car3()
print c.miles
c.miles = 5
print c.miles
*/

/* private variables

println "#4"
class Car4 {

private miles = 9

}

Car4 c = new Car4()
print c.miles
c.miles = 5  // private does not really take into effect unless we define a setter that overrides this
print c.miles
*/


/* discard setting of private variables
*/
println "#5"
class Car5 {

private miles = 9

def getMiles() { miles }

def setMiles(newMiles) {throw new IllegalAccessException("illegal exception") }

}

Car5 c = new Car5()
print c.miles
print c.getMiles()
c.miles = 5  // private does not really take into effect unless we define a setter that overrides this
print c.miles
c.setMiles(10)
print c.miles
