//add to a list the squares of all values from 1 to n
def f(n)
{
def list = []

for(int i=1; i<=n; i++)
{
    list << i*i
}

println list

}

f(1)
f(2)