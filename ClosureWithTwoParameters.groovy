def f(a,b, closure)
{
   closure(a,b)
}

f(3,5, {param1, param2 ->  println "$param1 * $param2" } )


///we can also provide optional typing
f(3,5, {param1, param2 ->  println "${param1 * param2}" } )
