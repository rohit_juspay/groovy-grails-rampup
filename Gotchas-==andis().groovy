String str1 = 'aaa'
String str2 = str1
String str3 = 'aaa'
String str4 = 'bbb'
String str5 = new String('aaa')

println "${str1 == str1}"
println "${str1 == str2}"
println "${str1 == str3}"
println "${str1 == str4}"
println "${str1 == str5}"

println "---------"
//reference doesn't work exactly in str1 and str3 case though both refering to same literal, need str5
println "${str1.is(str1)}"
println "${str1.is(str2)}"
println "${str1.is(str3)}"
println "${str1.is(str4)}"
println "${str1.is(str5)}"