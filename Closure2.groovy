a = {it % 2 == 0}

def f(n, closure){

for (int i=0; i<=n; i++)
{
 if(closure(i)) println i

}

}

f(10,a)//prints even

def f2(n, closure){

for (int i=0; i<=n; i++)
{
  //made typo here, evaluates as true itself
 if(closure) println i

}

}

f2(10, a)
