String[] greetings = ["Hello", "Hi", "Howdy"]

//for each when you need to specify the type
for(String greet : greetings) {
println greet
}

//for each when you shouldn't specify type use 'in'
for(greet2 in greetings) {
println greet2
}