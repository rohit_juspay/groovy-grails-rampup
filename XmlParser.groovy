languages = new XmlParser().parse('languages.xml')
println "Languages and authors"

languages.each {
println "${it.@name} authored by ${it.author[0].text()}"
}

println "----"
def languagesByAuthor = { authorName ->
languages.findAll { it.author[0].text() == authorName }.collect {
it.@name }.join(', ')
}

languagesByAuthor 'Wirth'

