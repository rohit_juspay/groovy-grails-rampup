enum CoffeeSize {SHORT, SMALL, MEDIUM, LARGE, EXTRALARGE}

for(size in CoffeeSize)
{
    println size
}

//should have break statement in switch
def OrderCoffee(size){

 print "$size"
 switch(size)
 {
   case [CoffeeSize.SHORT, CoffeeSize.SMALL] : println "1"
                   break;
   case CoffeeSize.MEDIUM .. CoffeeSize.EXTRALARGE : println "2"
                   break;
 }
 
 
}

OrderCoffee(CoffeeSize.SHORT)
OrderCoffee(CoffeeSize.SMALL)
OrderCoffee(CoffeeSize.MEDIUM)
OrderCoffee(CoffeeSize.LARGE)
OrderCoffee(CoffeeSize.EXTRALARGE)