lst = [1, 3, 4, 1, 8, 9, 2, 6]
println lst
println lst.getClass().name

println lst[0]
println lst[lst.size() - 1]
println lst[-1]

println lst[2..4]
println "--------------------"
lst.each {println it}
println "--------------------"
lst.reverseEach {println it}
println "--------------------"
println lst.collect { it * 2 }
println "--------------------"
println lst.find { it == 2 }
println lst.findAll { it % 2 == 0 }
lst.findIndexOf {it % 2 == 0}