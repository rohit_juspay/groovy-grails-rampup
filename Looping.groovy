println "#1"
3.times {
print 1
}

println "\n#2"
for(i in 0..2) {print i}

println "\n#3"
0.upto(2){ print "$it "}

println "\n#4"
0.upto(2){ print it} //it will be applicable only if i is not present

println "\n#5"
0.upto(2){ print '$it'}  //single quotes treat word as such and does not evaluate

println "\n#6"
0.upto(2){ print "$it" } //evaluates it

println "\n#7"
0.upto(2){ print "$it " } //evaluates it

println "\n#8"
0.step(10, 2) { print "$it "}  // 0,2,4,6,8
