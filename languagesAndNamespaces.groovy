//to collect those in a namespace only
languages = new XmlSlurper().parse(
'computerAndNaturalLanguages.xml').declareNamespace(human: 'Natural', human2: 'Computer')

print "Languages: "
println languages.language.collect { it.@name }.join(', ')
print "Natural languages: "
println languages.'human:language'.collect { it.@name }.join(', ')
println languages.'human2:language'.collect { it.@name }.join(', ')