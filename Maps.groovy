map = ['inception':'nolan', 'departed': 'scorscesee', 'true romance': 'tony scott']
println map
println map['inception']
println map.'inception'
println map.inception //neat huh, if instead of 'inception' we have 'c++' cant have stuff like this

//can eliminate quotes in case of well-behaved names for keys alone NOT FOR VALUES
map2 = [inception:'nolan', departed: 'scorscesee', 'true romance': 'tony scott']
println map2
println map2['inception']
println map2.'inception'
println map2.inception //neat huh, if instead of 'inception' we have 'c++' cant have stuff like this

//mapentry is give
langs = ['C++' : 'Stroustrup', 'Java' : 'Gosling', 'Lisp' : 'McCarthy']
langs.each { entry ->
println "Language $entry.key was authored by $entry.value"
}

println langs.collect { language, author ->
language.replaceAll("[+]", "P")
}

println "Looking for the first language with name greater than 3 characters"
entry = langs.find { language, author ->
language.size() > 3
}

println "----------"
entry = langs.find { entity -> entity.key.size() > 3 }

print "Does any language name have a nonalphabetic character? "
//only handles values
println langs.any { language, author ->
language =~ "[^A-Za-z]"
}




